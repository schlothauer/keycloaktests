Keycloak protected Angualr 2 devserver

Attention: in some cases the keycloak javascript adapter runs into an endless loop. This
seems to be a problem of the npm devserver but need to be debuged.

```
cd AngularTests
docker-compose/devel/bin/start.sh &
cd testLogin
npm run devserver
# open browser of your choise an test the configured URLs
```
It's also possible to use HTTPS with keycloak tests. The only requirement is to add the
name 'keycloak-tests' to local host file. Name 'keycloak-tests' need to pointed to a real
ip-address of the local computer. An entry that connect this name with the loopback-device
will not work.

If you use docker-compose/devel/bin/start.sh to bring up the docker container, the needed
certs are created and deployed automatically. By default this certs are self signed and 
valid for 730 days. If you need to create new certs then simply remove the old one before you
call start.sh

```
# delete of old certificates and truststores
rm -f ./docker-compose/devel/httpd/data/ssl/apache.pem
rm -rf ./docker-compose/devel/httpd/data/ssl/*.0
rm -rf ./docker-compose/devel/tomcat/truststore/my_keystore.store
rm -rf ./docker-compose/devel/tomcat/truststore/apache.der
```


Replace YOUR_LOCAL_IP with the ip address of your computer, but don't use localhost or 127.0.0.1.

Protected by realm role (Tomcat servlet)
* http://YOUR_LOCAL_IP/eins/    
* User: lisa, marge

Protected by realm role (Angular App running in local node.js)
* http://YOUR_LOCAL_IP/zwei/    
* User: lisa, homer

Available user:
* lisa,  Pwd: IchBinLisa
* marge, Pwd: IchBinMarge
* homer, Pwd: IchBinHomer
* bart,  Pwd: IchBinBart

Keycloak-Admin-Console
* http://YOUR_LOCAL_IP/auth/admin/master/console
* User: batman, Pwd: IchBinBatman




