#!/bin/bash

# stop the test environment

scriptPos=${0%/*}

composeFile="$scriptPos/../docker-compose.yml"

docker-compose -f "$composeFile" down


rm -f $scriptPos/../httpd/data/ssl/apache.pem
rm -rf $scriptPos/../httpd/data/ssl/*.0
rm -f $scriptPos/../tomcat/truststore/my_keystore.store
rm -f $scriptPos/../tomcat/truststore/apache.der

sudo rm -rf "$scriptPos/../db/pg_data"
sudo rm -rf "$scriptPos/../tomcat/webapps"


